public class App {
    public static void main(String[] args) throws Exception {
        Autonoleggio autonoleggio = new Autonoleggio(1000);
    // Aggiungi un nuovo veicolo
    autonoleggio.aggiungiVeicolo(new Veicolo(0, "AA123BB", "Fiat", "Panda", 1200, 2010, 5));
    // Aggiungi un altro veicolo
    autonoleggio.aggiungiVeicolo(new Veicolo(0, "CC456DD", "Ford", "Fiesta", 1400, 2012, 5));
    // Stampa tutti i veicoli
    autonoleggio.stampaVeicoli();
    System.out.println(autonoleggio.getPostiLiberi());
    }
}
