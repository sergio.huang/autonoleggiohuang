public class Veicolo {
    private int codice;
    private String targa;
    private String marca;
    private String modello;
    private int cilindrata;
    private int annoDiAcquisto;
    private int numeroDiPosti;

    public Veicolo() {
    }

    public Veicolo(int codice, String targa, String marca, String modello, int cilindrata, int annoDiAcquisto, int numeroDiPosti) {
        this.codice = codice;
        this.targa = targa;
        this.marca = marca;
        this.modello = modello;
        this.cilindrata = cilindrata;
        this.annoDiAcquisto = annoDiAcquisto;
        this.numeroDiPosti = numeroDiPosti;
    }

    public int getCodice() {
        return this.codice;
    }

    public void setCodice(int codice) {
        this.codice = codice;
    }

    public String getTarga() {
        return this.targa;
    }

    public void setTarga(String targa) {
        this.targa = targa;
    }

    public String getMarca() {
        return this.marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModello() {
        return this.modello;
    }

    public void setModello(String modello) {
        this.modello = modello;
    }

    public int getCilindrata() {
        return this.cilindrata;
    }

    public void setCilindrata(int cilindrata) {
        this.cilindrata = cilindrata;
    }

    public int getAnnoDiAcquisto() {
        return this.annoDiAcquisto;
    }

    public void setAnnoDiAcquisto(int annoDiAcquisto) {
        this.annoDiAcquisto = annoDiAcquisto;
    }

    public int getNumeroDiPosti() {
        return this.numeroDiPosti;
    }

    public void setNumeroDiPosti(int numeroDiPosti) {
        this.numeroDiPosti = numeroDiPosti;
    }

    public Veicolo codice(int codice) {
        setCodice(codice);
        return this;
    }

    public Veicolo targa(String targa) {
        setTarga(targa);
        return this;
    }

    public Veicolo marca(String marca) {
        setMarca(marca);
        return this;
    }

    public Veicolo modello(String modello) {
        setModello(modello);
        return this;
    }

    public Veicolo cilindrata(int cilindrata) {
        setCilindrata(cilindrata);
        return this;
    }

    public Veicolo annoDiAcquisto(int annoDiAcquisto) {
        setAnnoDiAcquisto(annoDiAcquisto);
        return this;
    }

    public Veicolo numeroDiPosti(int numeroDiPosti) {
        setNumeroDiPosti(numeroDiPosti);
        return this;
    }

    public String toString() {
        return "codice=" + getCodice()+
            ", targa=" + getTarga() +
            ", marca=" + getMarca() +
            ", modello=" + getModello() +
            ", cilindrata=" + getCilindrata() + 
            ", annoDiAcquisto=" + getAnnoDiAcquisto() +
            ", numeroDiPosti=" + getNumeroDiPosti();
    }
}