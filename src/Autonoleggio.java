public class Autonoleggio {
    private Veicolo[] veicoli;
    private int numeroVeicoli;
    private int maxVeicoli;

    public Autonoleggio(int maxVeicoli) {
        this.veicoli = new Veicolo[maxVeicoli];
        this.numeroVeicoli = 0;
        this.maxVeicoli=maxVeicoli;
    }

    public void aggiungiVeicolo(Veicolo v) {
        if (numeroVeicoli < veicoli.length) {
            veicoli[numeroVeicoli++] = v;
        } else {
            System.out.println("Non ci sono più posti disponibili per aggiungere nuovi veicoli.");
        }
    }

    public void stampaVeicoli() {
        for (int i = 0; i < numeroVeicoli; i++) {
            System.out.println(veicoli[i]);
        }
    }

    public int getPostiLiberi(){
        return maxVeicoli-numeroVeicoli;
    }
}